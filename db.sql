create database practica;

use practica;

create table User(

    name varchar(100) not null primary key,
    age int not null,
    user varchar(20) not null,
    password varchar(100) not null
)
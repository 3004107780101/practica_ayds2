import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import dotenv from 'dotenv';

import userRoutes from '../routes/users.js'

// INITIALIZE =====================================
const app = express();
import './db.js'
dotenv.config();
//=================================================


// SET PORT =======================================
app.set('port', process.env.PORT || 3500);
//=================================================


//MIDDLEWARES =====================================
app.use(morgan('method :url :status: :res[content-length] - :response-time ms'));
app.use(express.urlencoded({ extended: false }))
app.use(express.json());
app.use(cors());
//=================================================


// ROUTES =========================================

app.use(userRoutes);

app.use((req, res, next) => {
    res.status(404).send('404 Not Found');
});
//=================================================



// STATICS ========================================
//=================================================



export default app;
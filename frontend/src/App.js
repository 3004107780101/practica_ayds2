import './App.css';
import './bootstrap.min.css';

import { BrowserRouter as Router, Route } from "react-router-dom";


import LogIn from './views/login';
import Register from './views/register';
import Home from './views/home';

function App() {
  return (
    <div>

      <Router>

        <Route exact path="/home/:user?" component={Home} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/" component={LogIn} />

      </Router>
    </div>
  );
}

export default App;

import React, { Component, Fragment } from 'react'

import axios from 'axios';

import HOST from '../HOST';

class Register extends Component {

    constructor(props) {

        super(props);
        this.state = {
            name: "",
            age: "",
            user: "",
            password: ""
        }

        this.handleChange = this.handleChange.bind(this);
        this.register = this.register.bind(this);
    }


    async register(e) {

        e.preventDefault();
        axios.post(HOST + "/register", this.state)
            .then((res) => {
                if (res.status === 200) {
                    alert("Registro exitoso")
                    this.props.history.push("/")
                } else {
                    alert("Registro fallido")
                }
            })
            .catch((err) => {
                alert("Registro fallido")
            })

    }



    async handleChange(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }


    render() {

        return (
            <Fragment>

                <div style={{ textAlign: "right", margin: 20, cursor: "pointer" }} onClick={() => this.props.history.push("/")}>
                    <h3>
                        Practica - 201712350
                    </h3>
                </div>
                <div style={{ textAlign: "center", margin: 20 }}>
                    <h1>
                        Registro de datos
                    </h1>
                </div>

                <div className="card card-body" style={{ alignItems: "center", marginBottom: 20 }}>
                    <div style={{ width: "40%" }}>

                        <form onSubmit={this.register} className="was-validated">

                            <div className="form-group">
                                <label htmlFor="name">Nombre:</label>
                                <input type="text" className="form-control" placeholder="" name="name" required onChange={this.handleChange} />
                                <div className="valid-feedback">Valid.</div>
                                <div className="invalid-feedback">Please fill out this field.</div>
                            </div>
                            <div className="form-group">
                                <label htmlFor="dpi">Edad:</label>
                                <input type="number" className="form-control" placeholder="" name="age" maxLength="3" required onChange={this.handleChange} />
                                <div className="valid-feedback">Valid.</div>
                                <div className="invalid-feedback">Please fill out this field.</div>
                            </div>
                            <div className="form-group">
                                <label htmlFor="email">Usuario:</label>
                                <input type="text" className="form-control" placeholder="" name="user" required onChange={this.handleChange} />
                                <div className="valid-feedback">Valid.</div>
                                <div className="invalid-feedback">Please fill out this field.</div>
                            </div>
                            <div className="form-group">
                                <label htmlFor="pass">Contraseña:</label>
                                <input type="password" className="form-control" placeholder="Contraseña" name="password" required onChange={this.handleChange} />
                                <div className="valid-feedback">Valid.</div>
                                <div className="invalid-feedback">Please fill out this field.</div>
                            </div>

                            {/*<button type="submit" className="btn btn-success"> enviar </button>*/}
                            <button type="submit" className="btn btn-primary" > Registrarse </button>
                        </form>
                    </div>
                </div>

            </Fragment>

        )

    }


}


export default Register;
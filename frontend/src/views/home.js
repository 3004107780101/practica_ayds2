



const Home = function (props) {

    if (props.match.params.user === undefined) {
        return (
            <div className="App">
                <div className="btn btn-info" onClick={() => { props.history.push('/') }}>
                    Go To Login :)
                </div>
            </div>
        )
    }
    else {
        return (
            <div className="App">
                <header className="App-header">
                    <h1>
                        Bienvenido - {props.match.params.user}!
                    </h1>
                </header>
            </div>
        )
    }


}

export default Home;
import React, { Component, Fragment } from 'react'

import axios from 'axios';

import HOST from '../HOST';

class LogIn extends Component {

    constructor(props) {

        super(props);
        this.state = {
            user: "",
            password: ""
        }

        this.handleChange = this.handleChange.bind(this);
        this.logIn = this.logIn.bind(this);
    }

    componentDidMount() {
        //console.log(this.props)
    }

    async logIn(e) {


        e.preventDefault();
        axios.post(HOST + "/login", this.state)
            .then((res) => {
                if (res.status === 200) {
                    this.props.history.push("/home/" + this.state.user)
                } else {
                    alert("Credenciales incorrectas")
                }
            })
            .catch((err) => {
                alert("Credenciales incorrectas")
            })


    }


    async handleChange(e) {
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }


    render() {

        return (
            <Fragment>

                <div style={{ textAlign: "right", margin: 20 }}>
                    <h3>
                        Practica - 201712350
                    </h3>
                </div>

                <div style={{ textAlign: "center", margin: 20 }}>
                    <h1>
                        Iniciar sesión
                    </h1>
                </div>

                <div className="card card-body" style={{ alignItems: "center", marginBottom: 20 }}>
                    <div style={{ width: "40%" }}>
                        <div className="was-validated">
                            <form onSubmit={this.logIn}>
                                <div className="form-group">
                                    <label htmlFor="email">Usuario:</label>
                                    <input type="text" className="form-control" placeholder="Ingrese correo" name="user" required onChange={this.handleChange} />
                                    <div className="valid-feedback">Valid.</div>
                                    <div className="invalid-feedback">Please fill out this field.</div>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="pass">Contraseña:</label>
                                    <input type="password" className="form-control" placeholder="Contraseña" name="password" required onChange={this.handleChange} />
                                    <div className="valid-feedback">Valid.</div>
                                    <div className="invalid-feedback">Please fill out this field.</div>
                                </div>

                                {/*<button type="submit" className="btn btn-success"> enviar </button>*/}
                                <button className="btn btn-primary" type="submit"> Ingresar </button>
                            </form>
                        </div>
                    </div>
                    <a href="/register"> ¿Aun no tienes cuenta? Haz click aqui  </a>
                </div>


            </Fragment>

        )

    }


}


export default LogIn;
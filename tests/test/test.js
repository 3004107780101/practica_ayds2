const { expect } = require('chai');
let chai = require('chai');
let chaiHttp = require('chai-http');
const should = chai.should();

chai.use(chaiHttp);
const url = 'http://localhost:3500';

// *************** CARNET ***************
//=======================================
//=============== 201712350 =============
////=====================================
// **************************************


describe('Registro de usuario', () => {

    it('Successfull register', (done) => {
        chai.request(url)
            .post('/register')
            .send({
                name: "Helmut Efrain - 201712350",
                age: 21,
                user: "efrainAlv",
                password: "123"
            })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Error register', (done) => {

        chai.request(url)
            .post('/register')
            .send({
                name: "Helmut Efrain - 201712350",
                age: 21,
                user: "efrainAlv",
                password: "123"
            })
            .end(function (err, res) {
                expect(res).to.have.status(400);
                done();
            });
    });

});

describe('Login: ', () => {

    it('Successfull Login', (done) => {
        chai.request(url)
            .post('/login')
            .send({ user: "efra", password: '12345678' })
            .end(function (err, res) {
                expect(res).to.have.status(200);
                done();
            });
    });

    it('Error Login', (done) => {
        chai.request(url)
            .post('/login')
            .send({ user: "efra", password: '123' })
            .end(function (err, res) {
                expect(res).to.have.status(400);
                done();
            });
    });
});